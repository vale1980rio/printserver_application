﻿using Spire.Pdf;
using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Services;

namespace PrinterServer
{
    /// <summary>
    /// Descrizione di riepilogo per PrintService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Per consentire la chiamata di questo servizio Web dallo script utilizzando ASP.NET AJAX, rimuovere il commento dalla riga seguente. 
    // [System.Web.Script.Services.ScriptService]
    public class PrintService : System.Web.Services.WebService
    {

        [WebMethod]
        public string Print(byte[] file, string printername, string filename)
        {
            try
            {
                //Prendo la Cartella di destinazione dei file registrata nel web.config
                string folder = WebConfigurationManager.AppSettings["path"];

                //Conrollo se esiste e se non c'è la creo
                if (!Directory.Exists(folder))
                {
                    Directory.CreateDirectory(folder);
                }

                //Creo la path di dove il metodo andrà a scrivere il file ricevuto
                var path = Path.Combine(folder, filename);

                // se il metodo 'CreateFile' ha creato il pdf allora lo mando in stampa nella stampante indicata
                if (CreateFile(file, path))
                {

                    PdfDocument doc = new PdfDocument();
                    doc.LoadFromFile(path);
                    doc.PrinterName = printername;
                    PrintDocument printDoc = doc.PrintDocument;
                    printDoc.Print();

                    string cancella =  WebConfigurationManager.AppSettings["cancellafile"];

                    //se il flag 'cancellafile' == true allora cancello il file creato.
                    if (cancella.ToLower() == "true")
                    {
                        DeleteFile(path);
                    }

                    return "Stampa Inviata Correttamente";
                }
                else
                {
                    return "Errore Creazione File Di Stampa.";
                }

               
            }
            catch (Exception e)
            {
                return e.Message;
            }

        }
        
        [WebMethod]
        public string Welcome()
        {
            return "Welcome To Printer Server";
        }
        
        private bool DeleteFile(string path)
        {
            try
            {

                if (File.Exists(path))
                {
                    File.Delete(path);
                }
                return true;
            }
            catch (Exception e)
            {

                return false;
            }

        }

        private bool CreateFile(byte[] file, string path)
        {
            try
            {
                File.WriteAllBytes(path, file);
                return true;
            }
            catch (Exception e)
            {

                return false;
            }

        }

    }
}
